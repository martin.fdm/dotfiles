

# ! Quitar de este archivo
# quitar el status bar con url en la parte inferior izquierda del navegador
# setear a true en about.config
toolkit.legacyUserProfileCustomizations.stylesheets 


# página para crear documentacion interactiva
readme.io


# ver comandos anteriores
# https://github.com/junegunn/fzf.git



# Info

# listas de mirrors en  : /etc/pacman.d/mirrorlist


# incorporar todos las configuraciones del usuario al root
# agregando /home/user en la fila del root en /etc/passwd,


# ver fotos del android phone:
# importar en shotwell y organizar










# Comandos


var=dato

set $var=dato

# se solicita que ingrese una var
read name_var
 
# o lo mejor: ingresa var en el mismo renglon

read -p ' solicita que ingrese var: ' name_var
echo $name_var

read -sp   silent+p  //no se ve lo que ingresa 

read -a    array 
echo ${names[0]}   ${names[1]}   ${names[2]}    

read 
echo $REPLY
# $REPLY es una variable de entorno donde se guarda cuando el input de read no tiene arg1

rmdir  directorio               vacios

# borrar recursivamente los directorios mencionados
rmdir -p /dir1/dir2        
# convieve estar en el directorio padre para eliminar ; tienen que estar vacios dir1 y dir2

tar -cvf   -create -verbose  -f=what file to compress
tar -xvf    -uncompress  //funciona con la compresion xz
high compression >> tar -cf /name-of-the-file.tar /things-to-compress ; xz -9e /name-of-the-file.tar

zip final.zip filename.txt

# setear la hr
timedatectl set-ntp true
 
# cambiar el nombre del dispositivo
hostnamectl set-hostname new_name 





# flashear usb (incluido windows iso)
woeusb
# flashear usb (sólo linux)
etcher



# 'cant allocate memory' problem solved by downloading Preload, and running it as root


# Info : 

# adding user 
 adduser name 
 usermod -aG sudo nameUser (add sudo privileges) 



# devuelve info del sistema, pc, cpu, gpu, audio, wifi
inxi -Fxz 



# network manager manjaro gui
nmtui

# conectar al wifi 
nmcli dev wifi connect <miSSID>
nmcli c up <miSSID> 



# activar el touchpad
instalar xorg-xinput (permite ver el nombre del touchpad)
instalar xf86-input-synaptics
cp /usr/share/X11/xorg.conf.d/"config del tocuhpad" /etc/X11




# basico para permitir acceso en un dispositivo y acceder con otro usando ssh
systemctl enable sshd.service
systemctl start sshd.service
ufw allow 22/tcp
ufw reload
ssh name@ip




# ejecutar comando como root en bash script 
sudo -u root <command>  


# devolver el exit status (0 o 1)
echo $?


# encontrar el keycode de una tecla  
xmodmap -pke | grep Tecla


# actualizar el index de archivos del sistema
sudo updatedb    

# para luego localizar archivos que coincidan con PatronDeBusqueda
locate PatronDeBusqueda



 cut -d+ -f 2,3 file.txt  <<< el output es igual al texto entre las columnas 2 y 3 delimitadas por el signo + 
 notese que el signo mas va inmediatamente seguido de la opcion -d, y -f es print out [imprimir] 


# get your public ip address
 wget -qO - icanhazip.com
 

 # ver manual de un comando en pdf 
 man -Tpdf command | zathura


 Actualizar UUID de particiones nuevas: en /etc/fstab y /etc/default/grub , despues ejecutar sudo update-grub


 pacman -Qi | grep -A 3 Nombre              para ver el nombre y la descripcion de paquetes instalados
 pacman -Qdtq    para mostrar los paquetes que no dependen de otros paquetes
 pacman -Qe      paquetes instalados por usuario
 pacman -Rdd     para forzar remove package
 

# activar fuentes en /usr/share/fonts/ 
fc-cache -f -v 


# setear zsh as default shell for user
sudo chsh -s /bin/zsh username


# no sé si me sirve esto
direccion de tplink http://192.168.0.254/   
# encontrar ip de dispositivo Tp-link       ... no me sirve
sudo nmap -sn 192.168.1.0/24 | grep -A 1 Tp-link


# saber qué librerias utiliza un programa  
ldd path/toProgram

# 'refrescar' los links a las librerias que se posean. Es como un source. 
ldconfig /usr/lib
# /usr/lib es el path default donde estarian ellas. pero puede ser cualquier path




# montar android phone en linux:
simple-mtpfs -l 
simple-mtpfs --device 1 dirToMount




