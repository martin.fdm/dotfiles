
#!/bin/zsh
## ALIASES



##	NixOS

alias actualizarNixOS="nix-env -u \'\*\'"
alias limpiarNixOSgeneraciones='sudo nix-collect-garbage -d'
alias listarNixOSgeneraciones='sudo nix-env -p /nix/var/nix/profiles/system --list-generations'
alias rebuildNixOS='sudo nixos-rebuild switch'

alias eliminarPaquete='nix-env -e'



##      Tmux

alias tl='tmux ls'
alias matarTmux_v1='pkill -f tmux'
alias matarTmux_v2='tmux kill-server'
alias matarTodasSesionesTmuxMenosEnfocada='tmux kill-session -a'
alias matarSesionEspecificaTmux='tmux kill-session -t $1'



##	Want you to do this:

alias ca="kill -10 $(pidof zsh)"
alias checki3barStatus='i3bar -V --bar_id=bar-0'
alias cs="$sss/cpu-data-Notify-send.sh"
alias ctc="cat $t/changes_made"
alias ctt="cat $t/todoList"
alias dat="echo -e 'systemd-analyze'; systemd-analyze;echo -e '\nlshw -short'; lshw -short; echo -e '\ninxi -Fx'; inxi -Fx"   ##ejecutar como root
alias dv='dirs -v'
alias ecm="echo -e '\n$1' >> $t/changes_made"
alias etp="echo -e '\n$1' >> $t/para-no-olvidar.txt"
alias ett="echo -e '\n$1' >> $t/todoList"
alias gdp="cat $HOME/Text\ Folder/2ndNode/3rdNode/4thNode/daju_server | xclip -i"
alias ggt="cat $HOME/Text\ Folder/2ndNode/3rdNode/4thNode/ggt | xclip -i"
alias ggt2="cat $t/2ndNode/3rdNode/tokenGitHub >1"
alias i3bc='$sss/updateI3blocksColors.sh'
alias kk="kill $(pidof /usr/lib/kdeconnectd) ; kill $(pidof kdeconnect-indicator)"
alias kkp='sudo rm /var/lib/pacman/db.lck'
alias limpiarSwap="su -c \"echo 3 >'/proc/sys/vm/drop_caches' && swapoff -a && swapon -a && printf \'\n%s\n\' \'Ram-cache and Swap Cleared\'\" root"
alias listarNixOSgeneraciones='sudo nix-env -p /nix/var/nix/profiles/system --list-generations'
alias listarServiciosCorriendo='sudo systemctl list-units --type=service --state=running'
alias listarPaquetesInstalados="LC_ALL=C pacman -Qi | awk '/^Name/{name=\$3} /^Installed Size/{print \$4\$5, name}' | sort -h"
alias lcc="sudo rm /var/cache/pacman/pkg/*"         #limpiar cache/pacman/pkg
alias lc='pc ; sc; lcc'
alias lcv='sudo rm $XDG_DATA_HOME/nvim/swap/*'        ##limpiar cache de nvim
alias lf='pacman -Q | grep $1'
alias mirrors='sudo pacman-mirrors --fasttrack 12 && sudo pacman -Syyu'
alias na='nm-applet >/dev/null 2>&1 & disown'
alias ns='$HOME/Scripts/zsh/newScript.zsh'
alias pc='echo Cantidad de paquetes en /var/cache/pacman/pkg;sudo ls /var/cache/pacman/pkg/ | wc -l' #muestra cantidad paquetes hay en la m cache
alias psg='ps aux | grep $1'
alias ro='rm Documento\ nuevo.docx Hoja\ de\ cÃ¡lculo\ nueva.xlsx PresentaciÃ³n\ nueva.pptx'
alias raw="echo \'awesome.restart()\'| awesome-client"
alias rp='ka polybar >/dev/null 2>&1 & disown; $sss/polybar_wrapper_for_i3wm.sh >/dev/null 2>&1 & disown'
alias sc='du -sh /var/cache/pacman/pkg/' 	#muestra el peso de los paquetes en la m cache
alias sendMail="echo "PRUEBA" | mail -s "TEST" $1" # con $1 como el email
alias sr='$ssh/screencastingActual.sh'
alias srp='source $HOME/.profile'
alias srx="source /etc/xprofile"
alias srz='exec zsh'
alias ssenable='sudo systemctl enable'
alias ssrestart='sudo systemctl restart'
alias ssstart='sudo systemctl start'
alias ssstop='sudo systemctl stop'
alias ssstatus='sudo systemctl status'
#alias sxc="show_colour $(xrdb -query | grep --regexp="*color*" | awk '{printf $2 " " }' | tr -d '#')"
alias sxc="show_colour $(cat $XDG_CACHE_HOME/wal/colors.sh | grep --regexp="color.*=" | awk -F "'"  '{printf $2 "  "}' | tr -d '#') "
alias ttcm='tt $t/changes_made'
alias ws='$sss/wallpaper_setter.zsh'
alias :q='exit'




##  Data transfer >      
#   epd=enter pen drive  upd=umount pen drive

alias epd='$sss/mount_usb.sh'
alias upd='$sss/umount_usb.sh'
########	Dot files Backup
alias bctx='$sss/cp_dot.files.sh'
alias bcu="sudo cp -r $t pdr/ && notify-send 'dot.files cp to pdr'"
alias fbup='bctx && sdf'
alias rv='rsync -vash $1 mnf@192.168.$2:$3'
alias sdf='$sss/send_dot.files.sh'




##	Programs

alias analizarBloat='ncdu -x $1'
alias ft='neofetch'
alias hf='./Scripts/fxltro.sh'
alias ii='setsid -f /opt/idea-IC-213.6777.52/bin/idea.sh >/dev/null 2>&1'
alias ki='setsid -f kdeconnect-indicator >/dev/null 2>&1'
alias mp='mplayer -v'
alias mpf='mplayer -fs'
alias mpr='mplayer -vf rotate=1'
alias mpv='setsid -f mpv'
alias pa='/opt/Postman\ Agent/Postman\ Agent >/dev/null 2>&1 & disown'
alias pacc='php artisan config:clear'
alias paoc='php artisan optimize:clear'
alias pas='php artisan serve --host 0.0.0.0 --port 8000'
alias pat='php artisan tinker'
alias restartLamp='sudo /opt/lampp/lampp restart'
alias sat='sudo flatpak run io.atom.Atom'
alias shf='./Scripts/filter.sh'
alias so='sudo setsid -f nemo'
alias spc='v .config/spicetify/config.ini'
alias spu='sudo spicetify update'
alias sss='sudo setsid -f subl; i3-msg [class="subl"] focus'
alias startLamp='sudo /opt/lampp/lampp start'
alias statusLamp='sudo /opt/lampp/lampp status'
alias stopLamp='sudo /opt/lampp/lampp stop'
alias vlc='setsid -f vlc $1 > /dev/null'
alias vm='virt-manager'
alias vs='/opt/vs-code/bin/code'
alias vw='setsid -f sxiv -fb $1 '        #look at an image folder from $1 (number)image
alias vwt='setsid -f sxiv -ftb'            #look at an image folder, must have an entry specified
alias xa='sudo /opt/lampp/manager-linux-x64.run >/dev/null 2>&1 & disown' 
alias xr='sudo /opt/lampp/xampp reload >/dev/null 2>&1 &' 
#alias xampp='/opt/lampp/manager-linux-x64.run ' 
alias yt='youtube-dl -ic'        #download video
alias yta='youtube-dl -xic'      #only audio
alias yv='youtube-viewer'
alias zt='setsid -f zathura'



##	Altered Commands

alias c='clear';   alias n='nnn'   alias sn='sudo nnn'
alias cat='bat'
alias crs="~/Scripts/sh/sys/clearRam\&Swap.sh"
alias h='nnn -H' ;   alias ..='cd ..'
alias ch='chmod 774'
alias df='df -h'        # human-readable sizes
alias ga='grep $1 $alias'
alias gcc='gcc -Wall'
alias g++='g++ -Wall'
alias grep='grep --colour=always'
alias grepa='grep -A'
alias grepb='grep -B'
alias grepc='grep -C'
alias egrep='egrep --colour=auto'
alias fgrep='fgrep --colour=auto'
alias faf='sudo find . -type f -name $1'
alias ff='diff -y $1 $2 --suppress-common-lines'
alias sff='sudo diff -y $1 $2 --suppress-common-lines'
alias ipa='ip -br a'
alias j='xfce4-terminal -e joplin -T joplin'
alias jd='xfce4-terminal -e joplin-desktop -T joplin-desktop'
alias k9='kill -9'
alias ka='killall'
alias killSession='pkill -u $USER'
alias l='ls -lh'                                                #large list
alias la='ls -hNa --color=auto --group-directories-first'	#all
alias lap='/usr/bin/ls -hNla --color=auto --group-directories-first'	#large list, all
alias ls='lsd  --color=auto --group-dirs=first'	#modified ls, nothing special
alias md='mkdir'
alias mdp='mkdir -p'
alias pg=pgrep
alias sv='sudo nvim';
alias sch='sudo chmod 774'
alias sctl='sudo systemctl'
alias scw='sudo chown mnf:mnf'
alias sfaf='sudo find . -type f -name $1'
alias smd='sudo mkdir'

#               move & remove     pacman
alias smv='sudo mv'
alias srm='sudo rm'
alias srmrf='sudo rm -rf'
alias srd='sudo rm -d'   #direct vacios
alias sps='sudo pacman -S'
alias spr='sudo pacman -Rns'
alias syu='sudo pacman -Syu && notify-send "Syu has finished"'

alias tt='truncate -s 0'
alias v='nvim'



##  Quick access

alias ac='nvim $XDG_CONFIG_HOME/awesome/rc.lua'
alias brc='nvim $XDG_CONFIG_HOME/bspwm/bspwmrc'
alias con='nnn $XDG_CONFIG_HOME'
alias f='nnn ~/Facultad/4toAnio/2doCuatr'
alias gr='sudo nvim /etc/lightdm/lightdm-gtk-greeter.conf'
alias i3c='nvim $XDG_CONFIG_HOME/i3/config'
alias i3b='sudo nvim $XDG_CONFIG_HOME/i3/i3blocks/i3blocks.conf'
alias ni='nnn $WALLPAPERS_DIR'
alias pno='nvim $t/para-no-olvidar.txt'
alias rc='nvim $HOME/.c/bash/.bashrc'
alias src='nvim $XDG_CONFIG_HOME/sxhkd/sxhkdrc'
alias sym='nvim $ZDOTDIR/systemMethods.zsh'
alias um='nvim $ZDOTDIR/userMethods.zsh'
alias va='nvim $ZDOTDIR/alias.zsh'
alias vars='nvim $ZDOTDIR/variables.zsh'
alias vg='nvim $gz'
alias vnc='sudo nvim /etc/nixos/configuration.nix'
alias vhc='nvim $XDG_CONFIG_HOME/hypr/hyprland.conf'
alias vp='nvim $HOME/.profile'
alias vrc='nvim $HOME/.vim/.vimrc'
alias vt='nvim $XDG_CONFIG_HOME/tmux/.tmux.conf'
alias xrc='nvim $XDG_CONFIG_HOME/sxiv/exec/key-handler'
alias z='sxiv $HOME/2ndNODE/fcd/plan_lic_sistemas.pdf'
alias zc='nvim $ZDOTDIR/.zshrc'


## BD

alias cpg='psql -U postgres -h localhost'
alias cmysql='mysql -h localhost -P 3333 -u root'

## System

alias svn="svn --config-dir $XDG_CONFIG_HOME/subversion"
alias wget=wget --hsts-file="$XDG_DATA_HOME/wget-hsts"
