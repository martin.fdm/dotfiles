#!/bin/sh


#   Git aliases and workflow's commands

alias gita='git add -A'
alias gitb='git branch'
alias gitc='git commit -m $1'
alias gitch='git checkout'
alias gitdf='git diff'
alias gitgc='git commit -am $1'
alias gits='git status'

alias gsl='git stash list'
alias gss='git stash save'
alias gsp='git stash pop'

alias gl='git log'
alias glp='git log --patch'
alias gl1='git log --pretty=oneline --abbrev-commit'



#   Modificar el historial de manera "interactiva"
# git rebase -i HEAD~n 




#   Managing dotFiles with git Bare

alias dtf='git --git-dir=$t/dotfiles --work-tree=$HOME'
alias sdtf='sudo dtf'

alias dfs='dtf status'
alias dfdf='dtf diff'
alias dfl='dtf log'

alias dflp='dtf log --patch'
alias dfl1='dtf log --pretty=oneline --abbrev-commit'


function exec_dfa() {
    dtf add $@
}

#   Add 1 file or multiples files with wildcard
alias dfa='exec_dfa'

#   Example:
# $sdfa $XDG_CONFIG_HOME/eww/file
# $sdfa $XDG_CONFIG_HOME/eww/*


#   Para actualizar los cambios de uno o multiples archivos rastreados
#   usaremos add -u
#   Observar que no espera ningun parametro. 
alias dfu='dtf add -u'

#   Basicos
alias dfc='dtf commit -m'

alias fetchDotfiles='dtf fetch'

alias pullDotfiles='dtf pull'

alias pushDotfiles='dtf push'

alias dfss='dtf stash save'



function exec_openDotfiles() {
    /opt/vs-code/bin/code $(dtf ls-tree --full-tree --name-only -r HEAD | sed 's! !\\ !g')
}

alias openDotfiles='exec_openDotfiles'

