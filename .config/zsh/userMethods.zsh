#!/bin/zsh



function gi() {
  curl -sLw "\n" https://www.toptal.com/developers/gitignore/api/\$@ ;
}


# crear archivos en .cache despues de usar Bleachbit
createDotCacheFiles () {
        mkdir /home/mnf/.cache/wal
        mkdir /home/mnf/.cache/zsh
        touch /home/mnf/.cache/wal/colors.sh
        touch /home/mnf/.cache/wal/sequences
        touch /home/mnf/.cache/wal/colors-tty.sh
        touch /home/mnf/.cache/zsh/dirs
}



# elimiar la maquina virtual de kvm
rvm() {
        sudo virsh undefine --domain $1
        sudo virsh destroy --domain $1
        sudo rm -f ~/2ndNODE/VMs/VROOM/"$1"Disk.raw
}




