#!/bin/zsh


cambiarHora(){
	sudo timedatectl set-time $1
}



#   cd + ls
function cl () {
	cd "$1"; lsd -h --group-directories-first
}



#   Limpiar cache clipmenu
ccm() {  
    local uid=$(id -u $user)
    sudo -u $USER truncate -s 0 /run/user/1000/clipmenu.6.mnf/line_cache ; sudo -u $USER rm /run/user/1000/clipmenu.6.mnf/[0-9]*
}



#   Elegir un comando para abrir su manual con zathura
cmap() { 
    man -k . | dmenu -l 30 | awk '{print $1}' | xargs -r man -Tpdf | setsid -f zathura - 
}


#   Autoexplcativo
limpiarJournalLogsTo2Months () {
	sudo journalctl --vacuum-time=2months
}



#   Deprecadisimo pero interesante notify-send
notify-send() {
    #Detect the name of the display in use
	local display=":$(ls /tmp/.X11-unix/* | sed 's#/tmp/.X11-unix/X##' | head -n 1)"
    #Detect the user using such display
	local user=$(who | grep '('$display')' | awk '{print $1}' | head -n 1)
    #Detect the id of the user
	local uid=$(id -u $user)
	sudo -u $USER DISPLAY=$display DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/$uid/bus notify-send "$@"
}





sz()    { du -sch $1 }


.sz()   { du --total -ach -d1 $1 }


ssz()   { sudo du -sch $1 }


.ssz()  { sudo du --total -ach -d1 $1 }


