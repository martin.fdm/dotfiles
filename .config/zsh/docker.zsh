
# A partir de un dockerfile, construimos una imagen

# docker build -t getting-started . 

# a run le podemos pasar una imagen del repo oficial de docker
# o una imagen nuestra

# docker run [container?s]

# docker logs [container]
# docker logs -f [container]

alias dp='docker ps'

alias dcu='docker-compose up -d'
alias dcd='docker-compose down'
alias dcre='docker-compose restart'
alias dcstart='docker-compose start'
alias dcstop='docker-compose stop'

alias de='docker exec'

function docker_exec_it {
    docker exec -it $1 /bin/sh
}

alias deit='docker_exec_it'

alias dsd='docker system df'


# docker - cleaning space

alias dsp='docker system prune'
alias dspa='docker system prune -a'

alias di='docker images'
alias dip='docker image prune'
alias dipa='docker image prune -a'

alias dcp='docker container prune'
alias dcpa='docker container prune -a'

alias dsp='docker system prune'
alias dspa='docker system prune -a'
