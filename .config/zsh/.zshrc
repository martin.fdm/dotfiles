# ~/.zshrc:

#       Measure file startup time
#zmodload zsh/zprof

[[ $- != *i* ]] && return


[ -f $HOME/.profile ] && source $HOME/.profile

#PYWAL Import colorscheme from 'wal' asynchronously
# &   # Run the process in the background.
# ( ) # Hide shell job control messages.

# (/bin/cat $XDG_CACHE_HOME/wal/sequences &)

# Alternative (blocks terminal for 0-3ms)
# /bin/cat $XDG_CACHE_HOME/wal/sequences

####. "~/.cache/wal/colors.sh"

# To add support for TTYs this line can be optionally added.
source $XDG_CACHE_HOME/wal/colors-tty.sh

#env vars for SO
trap "clear &" 10



source $ZDOTDIR/alias.zsh
source $ZDOTDIR/systemMethods.zsh
source $ZDOTDIR/userMethods.zsh
source $ZDOTDIR/variables.zsh
source $ZDOTDIR/git.zsh
source $ZDOTDIR/docker.zsh

source $ZDOTDIR/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh 2>/dev/null
source $ZDOTDIR/zsh-autosuggestions/zsh-autosuggestions.zsh 2>/dev/null


[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
zstyle ':completion:*' cache-path "$XDG_CACHE_HOME"/zsh/zcompcache
autoload -Uz compinit
compinit -d "$XDG_CACHE_HOME"/zsh/zcompdump-"$ZSH_VERSION"




### 	Settings

use_color=true
unset use_color safe_term match_lhs sh

xhost +local:root > /dev/null 2>&1






# Enable colors and change prompt:
autoload colors && colors

if [[ ${EUID} == 0 ]] ; then
    PS1="%B% %F{124}%   π  %1~ >_ %f%b "
else
    PS1="%B% %F{183} %  π  %1~ >_ %f%b "
fi




setopt extended_glob
setopt auto_cd

setopt AUTO_PUSHD PUSHD_SILENT PUSHD_TO_HOME

## Remove duplicate entries
setopt PUSHD_IGNORE_DUPS

### This reverts the +/- operators.
setopt PUSHD_MINUS

setopt hist_ignore_all_dups
setopt hist_ignore_space



# Basic auto/tab complete:
#autoload -U compinit
#zstyle ':completion:*' menu select
#zmodload zsh/complist

#compinit
#_comp_options+=(globdots)		# Include hidden files.


# vi mode
#bindkey -v
#export KEYTIMEOUT=1

# Use vim keys in tab complete menu:

#bindkey -M menuselect 'h' vi-backward-char
#bindkey -M menuselect 'k' vi-up-line-or-history
#bindkey -M menuselect 'l' vi-forward-char
#bindkey -M menuselect 'j' vi-down-line-or-history
#bindkey -v '^?' backward-delete-char



# Ghost functions


ex () {

    if [ -f $1 ] ; then
        case $1 in
          *.tar.bz2)   tar xjf $1   ;;
          *.tar.gz)    tar xzf $1   ;;
          *.bz2)       bunzip2 $1   ;;
          *.rar)       unrar x $1     ;;
          *.gz)        gunzip $1    ;;
          *.tar)       tar xf $1    ;;
          *.tbz2)      tar xjf $1   ;;
          *.tgz)       tar xzf $1   ;;
          *.zip)       unzip $1     ;;
          *.Z)         uncompress $1;;
          *.7z)        7z x $1      ;;
          *)           echo "'$1' cannot be extracted via ex()" ;;
        esac
    else
    echo "'$1' is not a valid file"
    fi
}




show_colour() {:
    perl -e 'foreach $a(@ARGV){print "\e[48:2::".join(":",unpack("C*",pack("H*",$a)))."m \e[49m @ARGV  \n"} ;print "\n"' "$@" 
    #perl -e 'foreach $a(@ARGV){print "\e[48:2::".join(":",unpack("C*",pack("H*",$a)))."m \e[49m  \n"} ;print "\n"' "$@"      --  untouched line 
}



#shopt -q -s cdspell
#shopt -s autocd # allows you to cd into directory merely by typing the directory name
#stty -ixon # Disable crt+s and crt+q on terminal




## Remembering recent directories

#autoload -Uz add-zsh-hook

#DIRSTACKFILE="${XDG_CACHE_HOME:-$HOME/.cache}/zsh/dirs"

#if [[ -f "$DIRSTACKFILE" ]] && (( ${#dirstack} == 0 )); then
#    dirstack=("${(@f)"$(< "$DIRSTACKFILE")"}")
#    [[ -d "${dirstack[1]}" ]] && cd -- "${dirstack[1]}"
#fi


#chpwd_dirstack() {
#	print -l -- "$PWD" "${(u)dirstack[@]}" > "$DIRSTACKFILE"
#}

# add-zsh-hook -Uz chpwd chpwd_dirstack

#DIRSTACKSIZE='20'





# Change cursor shape for different vi modes.
#function zle-keymap-select {

#    if [[ ${KEYMAP} == vicmd ]] ||
#        [[ $1 = 'block' ]]; then
#        echo -ne '\e[1 q'

#    elif [[ ${KEYMAP} == main ]] ||

#            [[ ${KEYMAP} == viins ]] ||
#            [[ ${KEYMAP} = '' ]] ||
#            [[ $1 = 'beam' ]]; then
#        echo -ne '\e[5 q'

#    fi

#}



# Use lf to switch directories and bind it to ctrl-o

#lfcd () {

#    tmp="$(mktemp)"
#    lf -last-dir-path="$tmp" "$@"
#    if [ -f "$tmp" ]; then
#        dir="$(cat "$tmp")"
#        rm -f "$tmp"
#        [ -d "$dir" ] && [ "$dir" != "$(pwd)" ] && cd "$dir"
#    fi
#}



#bindkey -s '^o' 'lfcd\n'



#zle -N zle-keymap-select
#zle-line-init() {

    # initiate `vi insert` as keymap (can be removed if `bindkey -V` has been set elsewhere)
#    zle -K viins 
#    echo -ne "\e[5 q"
#}

#zle -N zle-line-init


# Use beam shape cursor on startup.
echo -ne '\e[5 q' 

#preexec() { echo -ne '\e[5 q' ;} # Use beam shape cursor for each new prompt.



# Edit line in vim with ctrl-e:

autoload edit-command-line; zle -N edit-command-line

bindkey '^e' edit-command-line




vcolors() {

	local fgc bgc vals seq0

	printf "Color escapes are %s\n" '\e[${value};...;${value}m'
	printf "Values 30..37 are \e[33mforeground colors\e[m\n"
	printf "Values 40..47 are \e[43mbackground colors\e[m\n"
	printf "Value  1 gives a  \e[1mbold-faced look\e[m\n\n"

	# foreground colors
	for fgc in {30..37}; do
		# background colors
		for bgc in {40..47}; do
			fgc=${fgc#37} # white
			bgc=${bgc#40} # black

			vals="${fgc:+$fgc;}${bgc}"
			vals=${vals%%;}

			seq0="${vals:+\e[${vals}m}"
			printf "  %-9s" "${seq0:-(default)}"
			printf " ${seq0}TEXT\e[m"
			printf " \e[${vals:+${vals+$vals;}}1mBOLD\e[m"
		done
		echo; echo
	done

}


#       To measure startup time
#zprof
