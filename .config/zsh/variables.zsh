#!/bin/zsh




#   Facultad 
pri=~/Facultad/1erAnio
seg=~/Facultad/2doAnio
ter=~/Facultad/3erAnio
cua=~/Facultad/4toAnio
qui=~/Facultad/5toAnio

a=~/Facultad/3erAnio/1erCuatr/Actualidad\ Informatica
e=~/Facultad/2doAnio/1erCuatr/Estadistica\ II
f=~/Facultad/4toAnio/2doCuatr
bd=~/Facultad/3erAnio/1erCuatr/Base\ Datos
dw=~/Facultad/4toAnio/1erCuatr/Disenio\ Web
img=~/Facultad/3erAnio/1erCuatr/Ingenieria\ de\ Software\ II
io=~/Facultad/3erAnio/2doCuatr/Investigacion\ Operativa
m=~/Facultad/3erAnio/1erCuatr/Matematica\ IV
poo=~/Facultad/3erAnio/1erCuatr/POO\ I 
poo2=~/Facultad/3erAnio/2doCuatr/POO\ II
pro=~/Facultad/3erAnio/2doCuatr/TrabajoFinal/practica/proyecto
proDW=~/Facultad/4toAnio/1erCuatr/Disenio\ Web/practica/Integrador/proyecto
proPOOI=~/Facultad/3erAnio/1erCuatr/POO\ I/practica/integrador/integradorPOO
proPOOII=~/Facultad/3erAnio/2doCuatr/POO\ II/practica/proyecto
r1=~/Facultad/3erAnio/1erCuatr/Redes\ I
r2=~/Facultad/3erAnio/2doCuatr/Redes\ II
sd=~/Facultad/4toAnio/1erCuatr/Sistemas\ Distribuidos
tc=~/Facultad/4toAnio/1erCuatr/Teoria\ de\ la\ Computacion
tf=~/Facultad/3erAnio/2doCuatr/TrabajoFinal


#   Trabajo
bi=~/Trabajo/bivo
py=~/Trabajo/bivo_py
st=~/Trabajo/bivo_store
com=~/Trabajo/comedores/proyecto


#        Trabajo
bi=~/Trabajo/bivo
st=~/Trabajo/bivo_store





#        Proyectos
im=~/Coding/sistema


#	Sistema
ac=$XDG_CONFIG_HOME/awesome/rc.lua
brc=$XDG_CONFIG_HOME/bspwm/bspwmrc
c=$XDG_CONFIG_HOME
cap=~/Imágenes/ScrCap/
alias=$ZDOTDIR/alias.zsh
dotfiles=$t/dot.files
gz=$ZDOTDIR/git.zsh
hc=$XDG_CONFIG_HOME/hypr/hyprland.conf
i3b=$XDG_CONFIG_HOME/i3/i3blocks/i3blocks.conf
i3c=$XDG_CONFIG_HOME/i3/config
img=$HOME/Imágenes
nixosConfig=/etc/nixos/configuration.nix
s=~/Scripts/
sc=~/Scripts/C/
spy=~/Scripts/py/
spys=~/Scripts/py/sys/
src=$XDG_CONFIG_HOME/sxhkd/sxhkdrc
ssh=~/Scripts/sh/
sss=~/Scripts/sh/sys/
sv=mnf@192.168.
sym=$ZDOTDIR/systemMethods.zsh
sz=~/Scripts/zsh/
t=$HOME/Text\ Folder
xp=/etc/xprofile
vars=$ZDOTDIR/variables.zsh
zc=$ZDOTDIR/.zshrc


#   Others
pno=$t/para-no-olvidar.txt


