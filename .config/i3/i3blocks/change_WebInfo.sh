#!/bin/bash
[[ -z $1 ]] && point="pdr"

pgrep -x dmenu && exit
mountable=$(ip -br a | grep "UP" | awk '{print $1}')
[[ "$mountable" = "" ]] && exit 1
chosen=$(echo "$mountable" | dmenu -i -p "Que red monitorear?" | awk '{print $1}')
[[ "$chosen" = "" ]] && exit 1
sed -i s+"INTERFACE=$(awk '/INTERFACE/ {print $1}' .config/i3/i3blocks/i3blocks.conf | cut -d= -f 2)"+"INTERFACE=${chosen}"+g .config/i3/i3blocks/i3blocks.conf
sed -i s+$(awk '/grep -e/ {print $7}' .config/i3/i3blocks/lan.sh)+"${chosen}"+ .config/i3/i3blocks/lan.sh

