#!/bin/bash

if [ $(cat /sys/class/power_supply/BAT0/status) == "Charging" ] ; then
 	cat /sys/class/power_supply/BAT0/capacity | xargs -I {} echo "{}"   	
else	
	cat /sys/class/power_supply/BAT0/capacity	
fi
