# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, inputs, ... }:


{


## Use the unstable channel as the default channel
# nixpkgs.config = {
#   allowUnfree = true;
#   packageOverrides = pkgs: {
#     unstable = pkgs.nixUnstable;
#   };
# };

  # Set pkgs to the unstable channel
  #pkgs = pkgs.nixUnstable;


  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  # System related

  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.loader.efi.efiSysMountPoint = "/boot/efi";
  
  
  # Settings

  # Set your time zone.
  time.timeZone = "America/Argentina/Cordoba";

  # Select internationalisation properties.
  i18n.defaultLocale = "es_AR.utf8";

  # Configure console keymap
  console.keyMap = "la-latin1";

  # Fonts
  #fonts.fonts = with pkgs; [
	#jetbrains-mono
  #];
  fonts.fontconfig.defaultFonts.monospace = [ "JetBrains Mono" ];

  # GTK
#  gtk = {
#    enable = true;
#    theme = {
#      name = "Papirus";
#      package = pkgs.papirus-icon-theme;
#    };
#  };



  # Screen light
  programs.light.enable = true;


  # Networking

  networking.hostName = "miracle"; # Define your hostname.
  #networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Enable networking
  networking.networkmanager.enable = true;



  # User

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.mnf = {
    isNormalUser = true;
    description = "mnf";
    extraGroups = [ "networkmanager" "wheel" "audio" "video" "docker" ];
  };


  users.defaultUserShell = pkgs.zsh;
  environment.shells = with pkgs; [ zsh ];

  
  services = {

  	dbus.enable = true;

  	# Audio
        pipewire =  {
  		enable = true;
  		alsa.enable = true;
  		alsa.support32Bit = true;
  		pulse.enable = true;
  		# If you want to use JACK applications, uncomment this
		#jack.enable = true;
	};


	# Picom
	picom.enable = true;
	
	# Enable the OpenSSH daemon.
  	# openssh.enable = true;
  };

  # rtkit is optional but recommended
  security.rtkit.enable = true;



  # List services that you want to enable:

  services.xserver = { 
  	enable = true;


  	# Configure keymap in X11
    	layout = "latam";
	xkbVariant = "";

  	displayManager.sddm.enable = true;

	# enabling touchpad:
	libinput.enable = true;
	libinput.touchpad.naturalScrolling = false;
	libinput.touchpad.middleEmulation = true;
	libinput.touchpad.tapping = true;

  	windowManager.i3 = {
        	enable = true;
	  	package = pkgs.i3-gaps;
  		extraPackages = with pkgs; [
			dmenu
			i3status
			i3lock
			i3blocks
			xfce.xfce4-panel
		]; 
        };


  };

  # Link /libexec from derivations to /run/current-system/sw. Line provided by NixOs Wiki on i3 
  environment.pathsToLink = [ "/libexec" ]; 





  nix = {
    settings = {
      # substituters = [
      #   "https://mirrors.tuna.tsinghua.edu.cn/nix-channels/store"
      #   "https://cache.nixos.org/"
      # ];
      auto-optimise-store = true; # Optimise syslinks
    };
    gc = {
      automatic = true;
      dates = "weekly";
      options = "--delete-older-than 7d";
    };
    package = pkgs.nixVersions.unstable;
    #registry.nixpkgs.flake = inputs.nixpkgs;
    extraOptions = ''
      experimental-features = nix-command flakes
      keep-outputs          = true
      keep-derivations      = true
    '';
  };


  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;









  # Docker
  virtualisation.docker = {
  	enable    = true;
  	rootless = {
  		enable = true;
  		setSocketVariable = true;
	};
  };

	
  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [

	# system

	brightnessctl
	libinput
	j4-dmenu-desktop
	dunst
	sddm
	i3-gaps
	ffmpeg
	redshift
	bleachbit
	picom

	python311
	python310Packages.pip

	networkmanagerapplet

	shared-mime-info
	
		# xorg
		xorg.xbacklight
		xorg.xmodmap
		xorg.xev
		xorg.xhost


		# basics
		zip
		unzip
		gcc
		cmake

		# cli
		alsa-utils
		zsh
		tmux
		alacritty
		tilix
		wget
		git
		curl
		lm_sensors
		neofetch
		htop
		bat
		lsd
		most
		mlocate
		psmisc
		scrot
		sysstat

		# audio
		pulseaudio
		pavucontrol
		cadence

		# aestethics
		lxappearance
		font-awesome
		polybar
	
	# utils

	firefox
	chromium
	zathura
	clipmenu
	kdeconnect
	pywal
	qalculate-gtk
	libreoffice-still

		# files management
		nnn
		ncdu
		
		# text
		vscode
		neovim

		# images and video
		mpv
		sxiv
		gimp
		shotcut


	# dev
	docker
	docker-compose

	# audio and music
	audacity
	spotify


  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = { 
  #   enable = true;
  #   enableSSHSupport = true;
  # };











  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leavecatenate(variables, "bootdev", bootdev)
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "22.05"; # Did you read the comment?



}


