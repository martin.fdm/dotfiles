#!/usr/bin/bash

function script() {
  $HOME/.config/bspwm/scripts/${@} &
  return $?
}

#script borders
eww -c $XDG_CONFIG_HOME/eww/bar/ open bar
pgrep -x sxhkd > /dev/null || sxhkd &
picom -bCG &
nm-applet &
dunst &
clipmenud &
