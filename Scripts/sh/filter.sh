#!/bin/sh
var=$(date +%H'h'%M'm')

grep $(printf -- "-e %s " $@) mh/ff_history.txt | sort -r | uniq > mh/filtro-$var.txt
vim mh/filtro-$var.txt
