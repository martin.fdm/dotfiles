#!/bin/bash 

#Copy dot files to Text Folder/dot.files
#exec by 'bctx' alias
rsync -varu .config/i3/ Text\ Folder/dot.files/i3/ --exclude default.config
rsync -varu Scripts/ Text\ Folder/dot.files/Scripts/ 							
rsync -vau ~/.c/bash/.bashrc Text\ Folder/dot.files/
rsync -vau ".config/sxiv/exec/key-handler" Text\ Folder/dot.files/ 
rsync -vau /etc/lightdm/lightdm.conf Text\ Folder/dot.files/ 
rsync -vau /etc/lightdm/lightdm-gtk-greeter.conf Text\ Folder/dot.files/ 
rsync -vau .config/mimeapps.list Text\ Folder/dot.files/ 
rsync -vau .config/picom.conf Text\ Folder/dot.files/
rsync -vau .profile Text\ Folder/dot.files/
rsync -vau .apps/.vim/.vimrc Text\ Folder/dot.files/ 
rsync -vau .config/zathura/zathurarc Text\ Folder/dot.files/ 
rsync -vau .zshrc Text\ Folder/dot.files/

echo -e "\n\nCopy successfully done" 




