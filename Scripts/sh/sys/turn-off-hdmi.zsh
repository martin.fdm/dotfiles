#!/bin/bash


hdmi=$(xrandr --query | awk '/HD/ {print $1}')

i3blocksConfig=/home/mnf/.config/i3/i3blocks/i3blocks.conf



xrandr --output $hdmi --off && \

sed -i s+"min_width=.*"+"min_width=550"+g "$i3blocksConfig" && \

wal -R && \

i3-msg restart
