#!/bin/bash
[[ -z "$1" ]] && point="pdr" || point="$1"

pgrep -x dmenu && exit
mountable=$(lsblk -lp | grep "part $" | awk '{print $1, "(" $4 ")"}')
[[ "$mountable" = "" ]] && exit 1
chosen=$(echo "$mountable" | dmenu -i -p "Que unidad montar?" | awk '{print $1}')
[[ "$chosen" = "" ]] && exit 1
sudo -u $USER mount "$chosen" "$point" >/dev/null 2>&1 && nnn "$point"

