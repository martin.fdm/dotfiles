#!/bin/bash

actual=$(pactl info | awk '/Destino por defecto/ {print $4}')

Genius="alsa_output.pci-0000_00_14.2.analog-stereo"
monitor="alsa_output.usb-Focusrite_iTrack_Solo-00.analog-stereo"

if [ $actual = $Genius ] ; then
	pactl set-default-sink ${monitor}; exec dunstify "Focusrite iTrack Solo" 
else
        pactl set-default-sink ${Genius};  exec dunstify "Genius"
fi	
