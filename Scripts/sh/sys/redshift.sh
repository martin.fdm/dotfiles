#!/bin/sh


#   Toggle redshift

redshift_process=$(pidof redshift -m vidmode)

[[ $? == 0 ]] && kill $redshift_process      && exec dunstify "redshift OFF" 

redshift -m vidmode >/dev/null 2>&1 & disown && dunstify "redshift ON"




#   Turn redshift on if it's evening or night

# var=$(date +%H)
# if [ "$var" -gt 21 ] & [ "$var" -lt 07 ] ; then
# 	exec redshift -m vidmode >/dev/null 2>&1 & disown && notify-send "redshift ON"
# fi	
