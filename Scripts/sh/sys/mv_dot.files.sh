#!/bin/bash

#sed -i s/INTERFACE=wlp0s20f0u5/INTERFACE=wlo1/g Text\ Folder/dot.files/i3/i3blocks/i3blocks.conf 
#sed -i s+"NR==18, "+""+g Text\ Folder/dot.files/i3/i3blocks/temp.sh
#sed -i s+NR==17+NR==13+g Text\ Folder/dot.files/i3/i3blocks/lan.sh
#sed -i s+--zoom+--stretch+g Text\ Folder/dot.files/.profile

cp -v Text\ Folder/dot.files/.bashrc ~/.c/bash/
cp -v Text\ Folder/dot.files/.profile ~/
cp -v Text\ Folder/dot.files/.vimrc ~/.apps/.vim/
cp -v Text\ Folder/dot.files/i3/config ~/.config/i3/
cp -v Text\ Folder/dot.files/i3/i3blocks/* ~/.config/i3/i3blocks/
cp -v Text\ Folder/dot.files/Scripts/* ~/Scripts/
cp -v Text\ Folder/dot.files/key-handler ~/.config/sxiv/exec/key-handler
cp -v Text\ Folder/dot.files/mimeapps.list ~/.config/
cp -v Text\ Folder/dot.files/zathurarc ~/.config/zathura/
cp -v Text\ Folder/dot.files/.zshrc ~/
sudo cp -v Text\ Folder/dot.files/lightdm.conf /etc/lightdm/
sudo cp -v Text\ Folder/dot.files/lightdm-gtk-greeter.conf /etc/lightdm/

echo -e "\n\n Copy Successfully Done"
#test
