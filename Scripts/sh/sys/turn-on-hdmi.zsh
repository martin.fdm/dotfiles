#!/bin/zsh

hdmi=$(xrandr --query | awk '/HD/ {print $1}')
lvds=$(xrandr --query | awk '/LVD/ {print $1}')

i3blocksConfig=/home/mnf/.config/i3/i3blocks/i3blocks.conf


xrandr --output $hdmi --left-of $lvds --mode 1920x1080 && \

sed -i s/'min_width=.*'/'min_width=850'/g ${i3blocksConfig} && \

wal -R && \

i3-msg restart

