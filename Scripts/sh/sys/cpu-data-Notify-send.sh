#!/bin/sh

cpu_usage=$(mpstat -T | grep all | awk '{print " " 100-$12 "%" }')
temp=$(~/.config/i3/i3blocks/temp.sh)
ram=$(free -m | awk '/^Mem:/ {print ($2-$7)/1024}' | xargs printf "%.2fGi\n")
lan=$(~/.config/i3/i3blocks/lan.sh)
swap=$(free -h | awk '/^Swap:/ {print $3}')
availablePacmanPackagesNumber=$(/usr/bin/cat $XDG_DATA_HOME/availablePacmanPackagesNumber)


dunstify "\
   $cpu_usage
        $ram
V    $swap
         $temp  
    lan $lan  
   $availablePacmanPackagesNumber"

