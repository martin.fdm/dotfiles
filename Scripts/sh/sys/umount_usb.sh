#!/bin/bash

[[ -z $1 ]] && sudo umount pdr && exit 0 || sudo umount "$1" && exit 0
