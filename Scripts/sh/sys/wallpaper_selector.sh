#!/bin/bash 

# Este script permitirá elegir especificamente entre mis wallapers favoritos, 
# encapsulando el nombre del archivo dentro de un nombre "interesante" 
# que lo referencie




# listamos el directorio con wallpapers
image=$(du --exclude==$WALLPAPERS_DIR/icons -a $WALLPAPERS_DIR | \ 

   #extraemos solo el nombre del archivo
  cut -f2- | cut -d/ -f6 | \

  # permitimos seleccion de la imagen con dmenu y ejecuctamos wal
  dmenu -i -l 30 )


# solicitamos el backend (o stilo)
backend=$()


# corremos wal
wal --backend $backend -o /home/mnf/Scripts/sh/sys/updateI3blocksColors.sh -i $WALLPAPERS_DIR/$image
  
