#!/bin/zsh 

#   Pasos:

#   listamos el directorio con wallpapers
#   extraemos solo el nombre del archivo
#   permitimos seleccion de la imagen con dmenu y ejecuctamos wal







image=$(du --exclude={$WALLPAPERS_DIR/icons,$WALLPAPERS_DIR/2ndNODE,$WALLPAPERS_DIR/webp,$WALLPAPERS_DIR/notebookResolution} -a $WALLPAPERS_DIR | cut -f2- | cut -d/ -f6 | sort | dmenu -i -l 30)


# manejo de error
[ -z $image ] && exit 1

# solicitamos el backend (o estilo)
posibles_backends=(haikoshu colorz colorthief)



# necesitamos pasarle a dmenu los elementos de la lista separados
backend=$(echo "${posibles_backends[@]}" | tr " " "\n" | dmenu -i -p "What backend would you prefer?")


# manejo de error
[ -z $backend ] && [ ! -z $image ] && backend='haikoshu'


# corremos wal
wal --backend $backend -o /home/mnf/Scripts/sh/sys/updateI3blocksColors.sh -i $WALLPAPERS_DIR/$image

