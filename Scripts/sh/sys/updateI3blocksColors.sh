#!/bin/bash





: ' this is comment block 

if [ "$1" = "on" ] ; then
		notify-send "" ; exec  >/dev/null 2>&1 &
elif [ "$1" = "q" ] ; then
		([ -z "$(pidof redshift -m vidmode)" ] && notify-send "Nop, redshift's OFF" ) \
		|| notify-send "Yep! redshift's ON"
else
		kill "$(pidof redshift -m vidmode)" && notify-send "Turning redshift OFF"
	fi

'





#declare -r colorElegido=$(xrdb -query | grep --regexp="*color15:" | awk '{printf $2}')
#sed -i s+^$formato.*+$formato${colorElegido}+g ~/.config/i3/i3blocks/i3blocks.conf


# puede   definir o/ pasar un color a este programa para pasar a i3blocks
#declare -r colorElegido="#003333"  #celesteHOscuro


## methods


show_colour() {
    perl -e 'foreach $a(@ARGV){print "\e[48:2::".join(":",unpack("C*",pack("H*",$a)))."m \e[49m "};print "\n"' "$@"
}



quitarAsterisco () {

    string=$1
    sinAsterisco="${string:1}"

    echo $sinAsterisco

}







# Definimos path de files a editar

i3blocksPath=~/.config/i3/i3blocks/i3blocks.conf

spicetifyThemeFilePath=~/.config/spicetify/Themes/Dribbblish/color.ini

dunstFilePath=~/.config/dunst/dunstrc 






# Definamos colores 



#colores="$(grep --regexp="color.*="\'"" ~/.cache/wal/colors.sh | cut -d# -f2 )"
: '
i=0

for color in ${colores[@]}  
do
    #echo ${color%?}
    color$i="#${color%?}"
    echo ${color$i}
    let "i+=1"
done

'


background="$(xrdb -query | grep --regexp="*color0:" | awk '{printf $2}')"
similarAlPrincipal="$(xrdb -query | grep --regexp="*color1:" | awk '{printf $2}')"
dominante="$(xrdb -query | grep --regexp="*color2:" | awk '{printf $2}')"
bordeMarcadorDeVentanaHijai3="$(xrdb -query | grep --regexp="*color3:" | awk '{printf $2}')"
bordeVentanas="$(xrdb -query | grep --regexp="*color4:" | awk '{printf $2}')"
posibilidadDosBordeVentana="$(xrdb -query | grep --regexp="*color5:" | awk '{printf $2}')"
posibilidadTresBordeVentana="$(xrdb -query | grep --regexp="*color6:" | awk '{printf $2}')"
muySecundario="$(xrdb -query | grep --regexp="*color7:" | awk '{printf $2}')"
similarAlMuySecundario="$(xrdb -query | grep --regexp="*color8:" | awk '{printf $2}')"








arrayColoresDefinidos=(

    $background
    $similarAlPrincipal
    $dominante
    $bordeMarcadorDeVentanaHijai3
    $bordeVentanas
    $posibilidadDosBordeVentana
    $posibilidadTresBordeVentana
    $muySecundario
    $similarAlMuySecundario

)









# Definimos formatos

formatoi3block="color="



arrayFormatoDunst=(

    "background=\""
    "foreground=\""
    "frame_color=\""

)




arrayFormatoSpicetify=(
    # cambia el fondo del texto hover, e iconos
    "main_fg="

    # background
    "main_bg="

    # text and other small stuff
    "secondary_fg=" 

    # barra lateral donde estan las playlists
    "secondary_bg=" 

    #bloque de arriba, lineas entre canciones, background dialog
    "sidebar_and_player_bg=" 

    #botones
    "indicator_fg_and_button_bg=" 
)



# TESTS

#quitarAsterisco ${background}

#FUNCIONA
#sed -i s+^"main_fg=".*+"main_fg="$(quitarAsterisco ${similarAlPrincipal})+g $spicetifyThemeFilePath


#test= echo $(quitarAsterisco ${background})

# IMPRIMR UN ELEMENTO DE ARRAY printf %q $arrayFormatoSpicetify


# IMPRIMIR COLORES DEL ARRAY DEFINIDO




: '

i=0

for color in ${arrayColoresDefinidos[@]} 
do  
    echo -n "color "$i, $color": ";show_colour ${color} 
    let "i+=1"
done

'

# IMPRIMIR ELEMENTOS DEL ARRAY Formato Spicetify
: '
for property in ${arrayFormatoSpicetify[@]}  
do
    echo $property

done

'


: '

for property in ${arrayFormatoDunst[@]}  
do
    echo $property

done

'




### ------------------------------------------------- ### 

# Elegimos los colores y editamos  i3blocks, dunst y spotify 



# i3blocks 

sed -i s+^$formatoi3block.*+$formatoi3block${muySecundario}+g $i3blocksPath





# Colores Seleccionados para DUNST y SPICETIFY

arrayColoresSeleccionadosDunst=(

    # background =
    $background

    # foreground =
    $bordeVentanas

    # frame_color =
    $posibilidadTresBordeVentana

)



: '                Recordamos las variables de colores:

background
similarAlPrincipal
dominante
bordeMarcadorDeVentanaHijai3
bordeVentanas
posibilidadDosBordeVentana
posibilidadTresBordeVentana
muySecundario
similarAlMuySecundario

'




arrayColoresSeleccionadosSpicetify=(

    # cambia el fondo del texto hover, e iconos
    #"main_fg="
    $bordeVentanas


    # background
    #"main_bg="
    $background


    # text and other small stuff
    #"secondary_fg=" 
    $muySecundario


    # barra lateral donde estan las playlists
    #"secondary_bg=" 
    $muySecundario


    #bloque de arriba, lineas entre canciones, background dialog
    #"sidebar_and_player_bg=" 
    $similarAlPrincipal


    #botones
    #"indicator_fg_and_button_bg=" 
    $bordeVentanas

)




# Editamos Dunstrc


i=0

for property in ${arrayFormatoDunst[@]} 

do  

    echo "$property${arrayColoresSeleccionadosDunst[$i]}\"" 
    sed -i s+".*$property".*+"$property"${arrayColoresSeleccionadosDunst[$i]}"\""+g $dunstFilePath
    let "i+=1"

done






# Editamos Spicetify


i=0

for property in ${arrayFormatoSpicetify[@]} 

do  

    colorSinAsterisco=$(quitarAsterisco ${arrayColoresSeleccionadosSpicetify[$i]})

    sed -i s+^"$property".*+"$property"${colorSinAsterisco}+g $spicetifyThemeFilePath
    let "i+=1"

done




killall dunst
spicetify update

i3-msg restart




killall /opt/spotify/spotify
j4-dmenu-desktop --dmenu="spotify" >/dev/null 2>&1 &


























: ' Colores del sistema obtenidos por $(xrdb -query)

                                Repite en color

*.color0:       #03142C               66             background. negro, hay poco en el wall
*.color1:       #EAAD79               9              un color similar a la tonalidad predominante (conforma el 10% del wallpaper )
*.color2:       #1E6D8B              10               un color dominante en el wallpaper. el mas oscuro de la tonalidad predominante
*.color3:       #5F6F88              11              me pa que este es el borde gris de la terminal que señala el child 
*.color4:       #2D94A8              12              los border claramente. este es el celeste
*.color5:       #5AA6B1              13              opcion 2 para los border muy parecido el celeste, mas claro que el color 12
*.color6:       #57BDC8              14              opcion 3 para los borders, mas claro que el 13
*.color7:       #d7d2ce              15              un gris muy claro, quiza lo elegio por una nube del wallpaper
*.color8:       #969390                              un gris claro tambien, posiblemente sea el border, pero mas no que si 
*.color9:       #EAAD79              1           
*.color10:      #1E6D8B              2               -

*.color11:      #5F6F88              3               -

*.color12:      #2D94A8              4               -

*.color13:      #5AA6B1              5               -
*.color14:      #57BDC8              6               -
*.color15:      #d7d2ce              7               -
*.color66:      #03142C              0          




'
