#!/bin/bash

# $1= output.file  &&   $2 fps rate 

#choose show only warning/error with:
#ffmpeg -loglevel warning -hide_banner -stats \
echo "####----------------Rec On-----------------####" ;
ffmpeg -loglevel error -hide_banner -stats \
 -f x11grab -s $(xdpyinfo | grep dimensions | awk '{print $2}') \
 -i :0.0 -r $2 \
 -f pulse -ac 2 -i $(pactl list sources | grep -A 2 "#0" | awk '/Nombre:/ {print $2}') \
 -f pulse -ac 1 -ar 44100 -i $(pactl list sources | grep -A 2 "#1" | awk '/Nombre:/ {print $2}') \
 -filter_complex amix=inputs=2 \
 -acodec libmp3lame -ar 44100 -q:a 1 \
 -c:v libx264 $1

# -f pulse -ac 2 -i $(pactl list sources | grep -A 2 "#1" | awk '/Name:/ {print $2}') \
# -f pulse -ac 1 -ar 44100 -i $(pactl list sources | grep -A 2 "#2" | awk '/Name:/ {print $2}') \
