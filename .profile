#! /bin/bash
# $HOME/.profile


export NNN_OPENER=xdg-open
export READER=/usr/bin/zathura
export EDITOR=/usr/bin/nvim
export TERMINAL=/usr/bin/alacritty
export QT_QPA_PLATFORMTHEME="qt5ct"
export QT_AUTO_SCREEN_SCALE_FACTOR=0
export GTK2_RC_FILES=$HOME/.gtkrc-2.0
export WALLPAPERS_DIR=$HOME/Imágenes/wall
export PAGER="most";

export NNN_USE_EDITOR=1

export NNN_BMS="h:$HOME/;i:$HOME/Imágenes;c:$XDG_CONFIG_HOME;d:$HOME/Descargas/;k:$HOME/2ndNODE/kdeConnect/;s:$HOME/Scripts/;x:$HOME/Text Folder/;a:$HOME/Facultad/3erAnio/1erCuatr/Actualidad Informatica/;b:$HOME/Facultad/3erAnio/1erCuatr/Base Datos/;e:$HOME/Facultad/2doAnio/1erCuatr/Estadistica II/;f:$HOME/Facultad/4toAnio/2doCuatr/;g:$HOME/Facultad/3erAnio/1erCuatr/Ingenieria de Software II/;m:$HOME/Facultad/3erAnio/1erCuatr/Matematica IV/;o:$HOME/Facultad/3erAnio/2doCuatr/Investigacion Operativa;p:$HOME/Facultad/3erAnio/1erCuatr/POO I/;r:$HOME/Facultad/3erAnio/1erCuatr/Redes I/;s:$HOME/Facultad/4toAnio/1erCuatr/Sistemas Distribuidos;t:$HOME/Facultad/4toAnio/1erCuatr/Teoria de la Computacion;w:$WALLPAPERS_DIR;z:$HOME/Facultad/3erAnio/2doCuatr/TrabajoFinal/practica/proyecto";

export NNN_PLUG='p:preview-tabbed'



export CM_HISTLENGTH=20;

export SPICETIFY_INSTALL=$HOME/2ndNODE/3rdNODE/spicetify-cli
export PATH="$PATH:$SPICETIFY_INSTALL"
export PATH="$PATH:$HOME/.local/bin"


# BASE DIR SPECIFICATIONS: // https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html

export XDG_DATA_HOME="$HOME/.local/share";
export XDG_CONFIG_HOME="$HOME/.config";
export XDG_STATE_HOME="$HOME/.local/state"

export XDG_RUNTIME_DIR="/run/user/$UID"
export XDG_DATA_DIRS="/usr/local/share:/usr/share";
export XDG_CONFIG_DIRS="/etc/xdg";
export XDG_TRASH_HOME="$XDG_DATA_HOME/Trash";

export XDG_CACHE_HOME="$HOME/.cache"



# MORE ENV VARS

export ZDOTDIR="$HOME"/.config/zsh
export XINITRC="$XDG_CONFIG_HOME"/X11/xinitrc
#export XAUTHORITY="$XDG_RUNTIME_DIR"/Xauthority       // este comando rompe el sistema por ahora
export CARGO_HOME="$XDG_DATA_HOME"/cargo
export GNUPGHOME="$XDG_DATA_HOME"/gnupg
export GTK2_RC_FILES="$XDG_CONFIG_HOME"/gtk-2.0/gtkrc
export XCURSOR_PATH=/usr/share/icons:${XDG_DATA_HOME}/icons
export LESSHISTFILE="$XDG_CACHE_HOME"/less/history
export ICEAUTHORITY="$XDG_CACHE_HOME"/ICEauthority
export MPLAYER_HOME="$XDG_CONFIG_HOME"/mplayer
export MYSQL_HISTFILE="$XDG_DATA_HOME"/mysql_history
export _JAVA_OPTIONS=-Djava.util.prefs.userRoot="$XDG_CONFIG_HOME"/java
export PSQL_HISTORY="$XDG_DATA_HOME/psql_history"
export PYTHONSTARTUP="${XDG_CONFIG_HOME}/python/pythonrc"
export BUNDLE_USER_CONFIG="$XDG_CONFIG_HOME"/bundle
export BUNDLE_USER_CACHE="$XDG_CACHE_HOME"/bundle
export BUNDLE_USER_PLUGIN="$XDG_DATA_HOME"/bundle
export RUSTUP_HOME="$XDG_DATA_HOME"/rustup
export SSB_HOME="$XDG_DATA_HOME"/zoom


export ERRFILE="$XDG_CACHE_HOME/X11/xsession-errors"

tmux source $XDG_CONFIG_HOME/tmux/.tmux.conf


export HISTFILE=$XDG_STATE_HOME/zsh/history
export HISTCONTROL=ignoreboth:erasedups
export HISTSIZE=10000
export SAVEHIST=10000
export HISTTIMEFORMAT="%F %T "

